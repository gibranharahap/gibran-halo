from django.db import models

class Status(models.Model):
	status = models.CharField(max_length=300)
	date = models.DateTimeField(auto_now=True)