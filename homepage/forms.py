from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
	status = forms.CharField(label = 'status',
								widget = forms.TextInput(attrs={"placeholder": "what's up mate?"}))
	
	class Meta:
		model = Status
		fields = ('status',
		)