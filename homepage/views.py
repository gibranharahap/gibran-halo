from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

def home(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/')
			
	else:
		form = StatusForm()
		input = Status.objects.all()
		
		return render(request, "home.html", {'form' : form, 'input' : input})