from django.test import TestCase, Client
from django.urls import resolve
from .forms import StatusForm
from .models import Status
from .views import home
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class HomeTest(TestCase):

    def test_home_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_home_not_exist(self):
        response = Client().get('/hi/')
        self.assertEqual(response.status_code, 404)
    
    def test_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
		
    def test_models_can_create_status(self):
        Status.objects.create(status = "ok", date = "now")
        hitungJumlah = Status.objects.all().count()
        self.assertEqual(hitungJumlah, 1)
	
    def test_views_post(self):
        data = {
            'status' : "OK",
            'date' : "now",
        }
        response = Client().post("", data)
        self.assertEqual(response.status_code, 302)


    def test_form_validation_for_blank_items(self):
        form = StatusForm(data = {'status': '',})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

class HomeFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')		
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(HomeFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(HomeFunctionalTest, self).tearDown()
		
    def test_title_page(self):
        self.selenium.get('http://127.0.0.1:8000/')
        self.assertEqual(self.selenium.title, "homey")

    def test_input(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('coba coba')
		
        # submitting the form
        submit.send_keys(Keys.RETURN)